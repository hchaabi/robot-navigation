# Overview

We are developing a system where a fleet of autonomous robots explores a disaster-stricken grid environment. Each robot uses reinforcement learning (RL) to independently navigate, communicate, and collaborate, enhancing movements and maintaining quality of service (QoS) without a central control unit.

# Assumptions

1. **Grid Environment:** The area is a grid where each cell can be passable or impassable due to obstacles or hazards.
2. **Robot Capabilities:** Robots are equipped with sensors for navigation and environmental assessment, have a limited communication range, and operate on finite battery life.

# Key Components

1. **State Space (S):** Each robot's state is defined as:
   - Position: `(x_i, y_i)`
   - Battery Level: `battery_i`
   - QoS Metric: `QosMetric_i`
   - Local Map: `map_i`

2. **Action Space (A):**
   - **Movement:** Robots can move to adjacent grid cells unless blocked.
   - **Connectivity Adjustments:** Robots adjust their connectivity to optimize QoS. Actions include `move_north`, `move_south`, `move_east`, `move_west`, and `adjust_connectivity`.

3. **Rewards (R):**
   - **Positive Rewards:** Gained by exploring new cells, maintaining or improving QoS, and efficient energy use.
   - **Negative Rewards:** Incurred by revisiting cells, experiencing poor QoS, and unnecessary energy consumption.

4. **Learning Algorithm:** Independent Deep Q-Network (DQN)
   - Each robot learns from personal experiences and selectively shared insights.
   - Policies are updated to maximize rewards based on: `Q(s, a) ← Q(s, a) + η * (r + λ * max_a' Q(s', a') - Q(s, a))`

5. **Exploration Strategy:** Epsilon-greedy
   - Begins with high randomness (high epsilon) and gradually shifts to learned optimal actions (lower epsilon).

6. **Communication and Sharing:**
   - Robots exchange significant experiences within their range to enhance collective learning and adapt to dynamic changes.

