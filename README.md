# Constraints

    - Scenario : Sending video streaming, sensors data, etc.
    - N_RNG(u) : the neighbor set of a robot u within a Relative Neighborhood Graph (RNG)

- ### RSSI:
  - **Description:** Maintain different minimum RSSI levels according to the scenario, ensuring robust communication within the Relative Neighborhood Graph (N_RNG) of each robot.
  - **Formula:** RSSI(u, v) ≥ RSSI<sub>min, scenario</sub> ∀u ∈ Robots, ∀v ∈ N_RNG(u)

- ### Throughput:
  - **Description:** Ensure different minimum throughput levels based on the scenario to handle varying data transmission needs, applicable within each robot's N_RNG.
  - **Formula:** Throughput(u, v) ≥ Throughput<sub>min, scenario</sub> ∀u ∈ Robots, ∀v ∈ N_RNG(u)

- ### Delay:
  - **Description:** Maintain different maximum allowable delay values depending on the scenario to ensure timely communication, focusing on the connections within each robot's N_RNG.
  - **Formula:** Delay(u, v) ≤ Delay<sub>max, scenario</sub> ∀u ∈ Robots, ∀v ∈ N_RNG(u)

- ### Movement:
  - **Description:** During each exploration iteration, at least one robot must advance to a new position that has not been previously explored by any robot in the fleet.
  - **Formula:** ∃u ∈ Robots: new_position(u, t+1) ∉ explored_areas<sub>previous</sub> for each time step t

- ### Energy Consumption Limit
  - **Description:** Each robot must optimize its energy usage to maximize its operational lifespan and coverage ability. The total energy consumed during an exploration should not exceed its battery capacity.
  - **Formula:** ∫<sub>0</sub><sup>t<sub>1</sub></sup> power_used(u, t) dt ≤ battery_capacity(u) ∀ u ∈ Robots, where t<sub>1</sub> is the time needed to execute the action

- ### Non-Collision Movement:
  - **Description:** Ensure all robots maintain a safe operational distance from each other to prevent any collisions.
  - **Formula:** distance(u, v) > safety_distance ∀ u, v ∈ Robots, u ≠ v

- ### Maximal Exploration:
  - **Description:** Optimize exploration coverage by continuously directing robots to unexplored areas, ensuring maximal area coverage while respecting QoS thresholds within each robot's N_RNG.
  - **Formula:** Each robot must explore new zones each iteration, ensuring no overlap with previously covered zones while maintaining QoS: RSSI, Throughput, Delay, within N_RNG.